var activity = null;
var samples = Array();
var distance = Array();
var altitude = Array();
var constant_altitude = Array();
var constant_altitude_element = 0.0;

function onload() {
    var layout = {
        legend: {
            x: 0.9,
            y: 1.0
        },
        xaxis: {
            // rangeselector: selectorOptions,
            rangeslider: {},
            title: {
                text: 'Distance (km)',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            },
        },
        yaxis: {
            title: {
                text: 'Elevation (m)',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            }
        }
    };
    trace1 = Array();
    trace2 = Array();
    Plotly.react(document.getElementById('aerolab'), [trace1, trace2], layout, {
        responsive: true
    });
}

function extractData() {
    window.samples = activity.records
    window.constant_altitude_element = activity.records[0].altitude * 1000.0;
}


function adjustEoffset() {
    return window.samples[0].altitude * 1000.0;
}

function createTraces() {
    if (window.auto_eoffset) {
        window.eoffset = adjustEoffset();
        document.getElementById("eoffset_slider").value = window.eoffset.toFixed(4);
        EOffsetOutput.innerHTML = window.eoffset.toFixed(4);
    }
    chungTheShitOutOfIt();
    var altitude;
    if (window.constant_elevation) {
        altitude = window.constant_altitude;
    } else {
        altitude = window.altitude;
    }
    var trace1 = {
        x: window.distance,
        y: window.virtual_elevation,
        name: "Virtual Elevation",
        type: 'scatter'
    };
    var trace2 = {
        x: window.distance,
        y: altitude,
        name: "Measured Elevation",
        type: 'scatter'
    };
    return [trace1, trace2];
}

function renderData(element) {
    [trace1, trace2] = createTraces();
    var layout = {
        legend: {
            x: 0.9,
            y: 1.0
        },
        title: {
            //text:'Chung Aero Analysis (beta)',
            text: window.activity_name,
            font: {
                family: 'Courier New, monospace',
                size: 24
            },
            xref: 'paper',
            x: 0.05,
        },
        xaxis: {
            // rangeselector: selectorOptions,
            rangeslider: {},
            title: {
                text: 'Distance (km)',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            },
        },
        yaxis: {
            title: {
                text: 'Elevation (m)',
                font: {
                    family: 'Courier New, monospace',
                    size: 18,
                    color: '#7f7f7f'
                }
            }
        }
    };
    Plotly.react(element, [trace1, trace2], layout, {
        responsive: true
    });
}


function updateData(element) {
    [trace1, trace2] = createTraces();
    var current_layout = element.layout;
    Plotly.deleteTraces(element, [0, 1]);
    Plotly.react(element, [trace1, trace2], current_layout, {
        responsive: true
    });
}
