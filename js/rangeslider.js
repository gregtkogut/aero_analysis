var CrrSlider = document.getElementById("crr_slider");
var CrrOutput = document.getElementById("crr_output");
var CdASlider = document.getElementById("cda_slider");
var CdAOutput = document.getElementById("cda_output");
var MassSlider = document.getElementById("mass_slider");
var MassOutput = document.getElementById("mass_output");
var RhoSlider = document.getElementById("rho_slider");
var RhoOutput = document.getElementById("rho_output");
var EtaSlider = document.getElementById("eta_slider");
var EtaOutput = document.getElementById("eta_output");
var EOffsetSlider = document.getElementById("eoffset_slider");
var EOffsetOutput = document.getElementById("eoffset_output");


CrrOutput.innerHTML = CrrSlider.value;
CdAOutput.innerHTML = CdASlider.value;
MassOutput.innerHTML = MassSlider.value;
RhoOutput.innerHTML = RhoSlider.value;
EtaOutput.innerHTML = EtaSlider.value;
EOffsetOutput.innerHTML = EOffsetSlider.value;


function auto_click() {
    window.auto_eoffset = document.getElementById('auto_eoffset').checked;
    updateData(document.getElementById('aerolab'));
}

function constant_click() {
    window.constant_elevation = document.getElementById('constant_elevation').checked;
    updateData(document.getElementById('aerolab'));
}

CrrSlider.oninput = function() {
    var value = Number(this.value).toFixed(4);
    CrrOutput.innerHTML = value;
    window.crr = value;
    updateData(document.getElementById('aerolab'));
}

CdASlider.oninput = function() {
    var value = Number(this.value).toFixed(4);
    CdAOutput.innerHTML = value;
    window.cda = value;
    updateData(document.getElementById('aerolab'));
}

MassSlider.oninput = function() {
    var value = Number(this.value).toFixed(2);
    MassOutput.innerHTML = value;
    window.mass = value;
    updateData(document.getElementById('aerolab'));
}

RhoSlider.oninput = function() {
    var value = Number(this.value).toFixed(4);
    RhoOutput.innerHTML = value;
    window.rho = value;
    updateData(document.getElementById('aerolab'));
}

EtaSlider.oninput = function() {
    var value = Number(this.value).toFixed(2);
    EtaOutput.innerHTML = value;
    window.eta = value;
    updateData(document.getElementById('aerolab'));
}

EOffsetSlider.oninput = function() {
    var value = Number(this.value).toFixed(2);
    EOffsetOutput.innerHTML = value;
    window.eoffset = value;
    updateData(document.getElementById('aerolab'));
}