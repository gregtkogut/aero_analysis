module.exports = {
    loadFITFile: loadFITFile
};

var activity = null;

var FitParser = require('./../../fit-parser/dist/fit-parser.js').default;

var fitParser = new FitParser({
    force: true,
    speedUnit: 'km/h',
    lengthUnit: 'km',
    temperatureUnit: 'kelvin',
    elapsedRecordField: true,
    mode: 'cascade',
});

function loadFITFile(content) {
    var activity = null;
    fitParser.parse(content, function(error, data) {
        if (error) {
            console.log(error);
        } else {
            console.log("FIT file successfully loaded.");
        }
        activity = data
    });
    return activity;
}
