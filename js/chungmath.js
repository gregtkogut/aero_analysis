var crr = 0.005;
var cda = 0.5;
var mass = 85.0;
var rho = 1.236;
var eta = 1.0;
var auto_eoffset = true;
var eoffset = 0.0;
var constant_elevation = false;
var virtual_elevation = Array();
const VFACTOR = 3.6;
const G = 9.80665;

function slope(f, a, m, crr, cda, rho, v) {
    return f / (m * G) - crr - cda * rho * v * v / (2.0 * m * G) - a / G;
}

function chungTheShitOutOfIt() {
    var vlast = 0.0;
    var dt = 1;
    var elevation = 0;
    var t = 0;
    var v = 0.0;
    var f = 0.0;
    var a = 0.0;

    window.virtual_elevation = Array();
    window.distance = Array();
    window.altitude = Array();
    window.constant_altitude = Array();
    window.samples.forEach(chungit)

    function chungit(sample) {
        v = sample.speed / VFACTOR;
        if (v > 0.01) {
            f = sample.power / v;
            a = (v * v - vlast * vlast) / (2.0 * dt * v);
        } else {
            a = (v - vlast) / dt;
        }
        if (isNaN(f)) {
          return;
        }

        f *= window.eta;
        var s = slope(f, a, window.mass, window.crr, window.cda, window.rho, v);
        var de = s * v * dt;
        elevation = elevation + de
        t = t + dt

        window.virtual_elevation.push(elevation + Number(window.eoffset));
        window.altitude.push(sample.altitude * 1000.0);
        window.constant_altitude.push(window.constant_altitude_element);
        window.distance.push(sample.distance);

        vlast = v
    }
}
